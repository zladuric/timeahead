/** TimeAhead - The tool to calculates at what time will an event occur when given the duration.
  * Copyright: Zlatko Đurić
  */
// dev only
var console = console || {
    log: function () {}
};
var TimeAhead = function() {
    this.flip = false;
    this.button = document.getElementById('calculate-button');
    this.inputEl = document.getElementById('input');
    // three helper methods I just googled
    function hasClass(ele,cls) {
        return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
    }
    function addClass(ele,cls) {
        if (!this.hasClass(ele,cls)) ele.className += " "+cls;
    }
    function removeClass(ele,cls) {
        if (hasClass(ele,cls)) {
            var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
            ele.className=ele.className.replace(reg,' ');
        }
    }
    this.onKeyUp = function TA_onKeyUp(e) {
        console.log(this);
        e.which = e.which || e.keyCode;
        if (e.which == 13) {
            this.calculate();
        }
    }
    /**
     * @method init - Set hooks on the calculate button. Loads up from a script tag, so we know the Dom is there.
     */
    this.init = function TA_init() {
        this.button = document.getElementById("calculate-button");
        this.button.setAttribute('onclick', 'TimeAhead.calculate()');
        this.inputEl.setAttribute('onkeyup', 'TimeAhead.onKeyUp(event)');
        this.inputEl.focus();
        return this;
    };
    /**
     *  @method calculate - gets the value and calculates time
     *  first, validate and then, calculate
     * expressions we work with (with spaces and commas ignored)
     *   Xd Xh Xm Xs
     *   X days X hours X minutes X seconds
     *   X days X hours X mins X secs
     *   d*:h?:mm:ss.???
    */
    this.calculate = function TA_calculate() {
        var dhmsPattern = /(([0-9]{1,3})(days|day|d){1})?(([0-9]{1,2})(hours|hour|h){1})?(([0-9]{1,2})(minutes|minute|mins|min|m){1})?(([0-9]{1,2})(seconds|second|secs|sec|s))?/i
            , days = 0
            , hours = 0
            , minutes = 0
            , seconds = 0
            , future
            , elem
            , h
            , m
            , s
            , day
            ;
/*  left out for now
        var hmsColonPattern = /([0-1]?[0-9]|2[0-3]):([0-5]?[0-9]):([0-5]?[0-9])/;
        var hmColonPattern = /([0-9]):([0-5]?[0-9])/;
*/
        this.results = document.getElementById('results');
        this.expression = this.inputEl.value;
        // first, nothing entered. Tell us so
        if (this.expression.length === 0) {
            console.log(this.flip);
            this.warning = document.getElementById('warning');
            this.warning.style.display = 'block';
            this.results.style.display = 'none';
            this.flip = true;
            return;
        }
        if (this.flip) {
            this.flip = false;
            this.warning.style.display = 'none';
        }
        this.expression = this.expression.replace(/\ and\ /g, '').replace(/ /g, '').replace(/,/g, '');
        this.matched = this.expression.match(dhmsPattern);
        if (this.matched[0] == '') {
            this.results.style.display = 'none';
            return; // no match
        }
        console.log(this.matched);
        if (this.matched[2] !== undefined) {
            console.log("days");
            days += parseInt(this.matched[2]);
        }
        if (this.matched[5] !== undefined) {
            hours += parseInt(this.matched[5]);
        }
        if (this.matched[8] !== undefined) {
            minutes += parseInt(this.matched[8]);
        }
        if (this.matched[11] !== undefined) {
            seconds += parseInt(this.matched[11]);
        }
        hours += days * 24;
        minutes += hours * 60;
        seconds += minutes * 60;
        future = new Date();
        future.setSeconds(future.getSeconds() + seconds);
        this.result = document.getElementById('result');
        day = future.getDay();
        elem = '';
        if (day === 0) {
            elem += 'Sunday';
        } else if ( day === 1) {
            elem += 'Monday';
        } else if ( day === 2) {
            elem += 'Tuesday';
        } else if ( day === 3) {
            elem += 'Wednesday';
        } else if ( day === 4) {
            elem += 'Thursday';
        } else if ( day === 5) {
            elem += 'Friday';
        } else if ( day === 6) {
            elem += 'Saturday';
        }
        elem += ', ';
        elem += '<strong>' + future.getDate() + '.' + (future.getMonth() + 1) + '.' + future.getFullYear() + '</strong>';
        h = future.getHours();
        m = future.getMinutes();
        s = future.getSeconds();
        
        elem += ', ';
        elem += '<strong>' + h + ':' + m + ':' + s + '</strong>';
        this.result.innerHTML = elem + '.';
        this.results.style.display = 'block';
    };
    return this;
};
